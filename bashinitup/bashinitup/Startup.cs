﻿using Microsoft.Owin;
using Owin;
[assembly: OwinStartupAttribute(typeof(bashinitup.Startup))]

namespace bashinitup
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
